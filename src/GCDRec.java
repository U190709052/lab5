
public class GCDRec {


    static int GCD(int a,int b) {


        int q = (a / b);
        int r = a - (q * b);

        if(r==0)
            return b;
        else
            return GCD(b, r);

    }


    public static void main(String[] args) {

        int number1 = Integer.parseInt(args[0]);

        int number2 = Integer.parseInt(args[1]);
        System.out.println(GCD(number1,number2));

    }
}